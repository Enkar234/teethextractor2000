﻿namespace TeethExtractor2000
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CalculateButton = new System.Windows.Forms.Button();
			this.OuterRTextBox = new System.Windows.Forms.TextBox();
			this.InnerCheckBox = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.XTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.ZLimitButton = new System.Windows.Forms.Button();
			this.ZLimitTextBox = new System.Windows.Forms.TextBox();
			this.Canvas = new System.Windows.Forms.PictureBox();
			this.ImportButton = new System.Windows.Forms.Button();
			this.YTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.ZTextBox = new System.Windows.Forms.TextBox();
			this.InnerRTextBox = new System.Windows.Forms.TextBox();
			this.OuterCheckBox = new System.Windows.Forms.CheckBox();
			this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.ProgressBar = new System.Windows.Forms.ProgressBar();
			((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
			this.TableLayoutPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// CalculateButton
			// 
			this.CalculateButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CalculateButton.Location = new System.Drawing.Point(1094, 3);
			this.CalculateButton.Name = "CalculateButton";
			this.CalculateButton.Size = new System.Drawing.Size(81, 34);
			this.CalculateButton.TabIndex = 18;
			this.CalculateButton.Text = "Calculate";
			this.CalculateButton.UseVisualStyleBackColor = true;
			this.CalculateButton.Click += new System.EventHandler(this.CalculateButton_Click_1);
			// 
			// OuterRTextBox
			// 
			this.OuterRTextBox.Location = new System.Drawing.Point(922, 5);
			this.OuterRTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.OuterRTextBox.Name = "OuterRTextBox";
			this.OuterRTextBox.Size = new System.Drawing.Size(43, 26);
			this.OuterRTextBox.TabIndex = 14;
			// 
			// InnerCheckBox
			// 
			this.InnerCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.InnerCheckBox.AutoSize = true;
			this.InnerCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.InnerCheckBox.ForeColor = System.Drawing.Color.White;
			this.InnerCheckBox.Location = new System.Drawing.Point(736, 8);
			this.InnerCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.InnerCheckBox.Name = "InnerCheckBox";
			this.InnerCheckBox.Size = new System.Drawing.Size(94, 24);
			this.InnerCheckBox.TabIndex = 16;
			this.InnerCheckBox.Text = "ON/OFF";
			this.InnerCheckBox.UseVisualStyleBackColor = true;
			this.InnerCheckBox.Click += new System.EventHandler(this.InnerCheckBox_Click);
			// 
			// label6
			// 
			this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label6.AutoSize = true;
			this.label6.ForeColor = System.Drawing.Color.Red;
			this.label6.Location = new System.Drawing.Point(846, 10);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(65, 20);
			this.label6.TabIndex = 13;
			this.label6.Text = "Outer R";
			// 
			// label5
			// 
			this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label5.AutoSize = true;
			this.label5.ForeColor = System.Drawing.SystemColors.Highlight;
			this.label5.Location = new System.Drawing.Point(606, 10);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(62, 20);
			this.label5.TabIndex = 11;
			this.label5.Text = "Inner R";
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label3.AutoSize = true;
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Location = new System.Drawing.Point(456, 10);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(20, 20);
			this.label3.TabIndex = 7;
			this.label3.Text = "Y";
			// 
			// XTextBox
			// 
			this.XTextBox.Location = new System.Drawing.Point(405, 5);
			this.XTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.XTextBox.Name = "XTextBox";
			this.XTextBox.Size = new System.Drawing.Size(40, 26);
			this.XTextBox.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label2.AutoSize = true;
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Location = new System.Drawing.Point(381, 10);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(16, 20);
			this.label2.TabIndex = 5;
			this.label2.Text = "X";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label1.AutoSize = true;
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(305, 10);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 20);
			this.label1.TabIndex = 4;
			this.label1.Text = "Center:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ZLimitButton
			// 
			this.ZLimitButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ZLimitButton.Location = new System.Drawing.Point(193, 3);
			this.ZLimitButton.Name = "ZLimitButton";
			this.ZLimitButton.Size = new System.Drawing.Size(99, 34);
			this.ZLimitButton.TabIndex = 3;
			this.ZLimitButton.Text = "Set Z cut";
			this.ZLimitButton.UseVisualStyleBackColor = true;
			this.ZLimitButton.Click += new System.EventHandler(this.ZLimitButton_Click);
			// 
			// ZLimitTextBox
			// 
			this.ZLimitTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ZLimitTextBox.Location = new System.Drawing.Point(95, 8);
			this.ZLimitTextBox.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
			this.ZLimitTextBox.Name = "ZLimitTextBox";
			this.ZLimitTextBox.Size = new System.Drawing.Size(92, 26);
			this.ZLimitTextBox.TabIndex = 2;
			this.ZLimitTextBox.Text = "0";
			// 
			// Canvas
			// 
			this.TableLayoutPanel.SetColumnSpan(this.Canvas, 17);
			this.Canvas.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Canvas.Location = new System.Drawing.Point(4, 45);
			this.Canvas.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Canvas.Name = "Canvas";
			this.Canvas.Size = new System.Drawing.Size(1170, 783);
			this.Canvas.TabIndex = 1;
			this.Canvas.TabStop = false;
			this.Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvas_Paint);
			this.Canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseDown);
			this.Canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseMove);
			this.Canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseUp);
			// 
			// ImportButton
			// 
			this.ImportButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ImportButton.Location = new System.Drawing.Point(3, 3);
			this.ImportButton.Name = "ImportButton";
			this.ImportButton.Size = new System.Drawing.Size(86, 34);
			this.ImportButton.TabIndex = 0;
			this.ImportButton.Text = "Import";
			this.ImportButton.UseVisualStyleBackColor = true;
			this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
			// 
			// YTextBox
			// 
			this.YTextBox.Location = new System.Drawing.Point(484, 5);
			this.YTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.YTextBox.Name = "YTextBox";
			this.YTextBox.Size = new System.Drawing.Size(37, 26);
			this.YTextBox.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label4.AutoSize = true;
			this.label4.ForeColor = System.Drawing.Color.White;
			this.label4.Location = new System.Drawing.Point(534, 10);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(18, 20);
			this.label4.TabIndex = 9;
			this.label4.Text = "Z";
			// 
			// ZTextBox
			// 
			this.ZTextBox.Location = new System.Drawing.Point(560, 5);
			this.ZTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.ZTextBox.Name = "ZTextBox";
			this.ZTextBox.Size = new System.Drawing.Size(32, 26);
			this.ZTextBox.TabIndex = 10;
			// 
			// InnerRTextBox
			// 
			this.InnerRTextBox.Location = new System.Drawing.Point(678, 5);
			this.InnerRTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.InnerRTextBox.Name = "InnerRTextBox";
			this.InnerRTextBox.Size = new System.Drawing.Size(42, 26);
			this.InnerRTextBox.TabIndex = 12;
			// 
			// OuterCheckBox
			// 
			this.OuterCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.OuterCheckBox.AutoSize = true;
			this.OuterCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.OuterCheckBox.ForeColor = System.Drawing.Color.White;
			this.OuterCheckBox.Location = new System.Drawing.Point(985, 8);
			this.OuterCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.OuterCheckBox.Name = "OuterCheckBox";
			this.OuterCheckBox.Size = new System.Drawing.Size(94, 24);
			this.OuterCheckBox.TabIndex = 17;
			this.OuterCheckBox.Text = "ON/OFF";
			this.OuterCheckBox.UseVisualStyleBackColor = true;
			this.OuterCheckBox.Click += new System.EventHandler(this.OuterCheckBox_Click);
			// 
			// TableLayoutPanel
			// 
			this.TableLayoutPanel.ColumnCount = 17;
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 98F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
			this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
			this.TableLayoutPanel.Controls.Add(this.OuterCheckBox, 15, 0);
			this.TableLayoutPanel.Controls.Add(this.InnerRTextBox, 11, 0);
			this.TableLayoutPanel.Controls.Add(this.ZTextBox, 9, 0);
			this.TableLayoutPanel.Controls.Add(this.label4, 8, 0);
			this.TableLayoutPanel.Controls.Add(this.YTextBox, 7, 0);
			this.TableLayoutPanel.Controls.Add(this.ImportButton, 0, 0);
			this.TableLayoutPanel.Controls.Add(this.Canvas, 0, 1);
			this.TableLayoutPanel.Controls.Add(this.ZLimitTextBox, 1, 0);
			this.TableLayoutPanel.Controls.Add(this.ZLimitButton, 2, 0);
			this.TableLayoutPanel.Controls.Add(this.label1, 3, 0);
			this.TableLayoutPanel.Controls.Add(this.label2, 4, 0);
			this.TableLayoutPanel.Controls.Add(this.XTextBox, 5, 0);
			this.TableLayoutPanel.Controls.Add(this.label3, 6, 0);
			this.TableLayoutPanel.Controls.Add(this.label5, 10, 0);
			this.TableLayoutPanel.Controls.Add(this.label6, 13, 0);
			this.TableLayoutPanel.Controls.Add(this.InnerCheckBox, 12, 0);
			this.TableLayoutPanel.Controls.Add(this.OuterRTextBox, 14, 0);
			this.TableLayoutPanel.Controls.Add(this.CalculateButton, 16, 0);
			this.TableLayoutPanel.Controls.Add(this.ProgressBar, 0, 2);
			this.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.TableLayoutPanel.Name = "TableLayoutPanel";
			this.TableLayoutPanel.RowCount = 3;
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TableLayoutPanel.Size = new System.Drawing.Size(1176, 863);
			this.TableLayoutPanel.TabIndex = 0;
			// 
			// ProgressBar
			// 
			this.TableLayoutPanel.SetColumnSpan(this.ProgressBar, 17);
			this.ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ProgressBar.Location = new System.Drawing.Point(3, 836);
			this.ProgressBar.Name = "ProgressBar";
			this.ProgressBar.Size = new System.Drawing.Size(1172, 24);
			this.ProgressBar.TabIndex = 19;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1176, 863);
			this.Controls.Add(this.TableLayoutPanel);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
			this.TableLayoutPanel.ResumeLayout(false);
			this.TableLayoutPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button CalculateButton;
		private System.Windows.Forms.TextBox OuterRTextBox;
		private System.Windows.Forms.CheckBox InnerCheckBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox XTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button ZLimitButton;
		private System.Windows.Forms.TextBox ZLimitTextBox;
		private System.Windows.Forms.PictureBox Canvas;
		private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
		private System.Windows.Forms.CheckBox OuterCheckBox;
		private System.Windows.Forms.TextBox InnerRTextBox;
		private System.Windows.Forms.TextBox ZTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox YTextBox;
		private System.Windows.Forms.Button ImportButton;
		private System.Windows.Forms.ProgressBar ProgressBar;
	}
}

