﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace TeethExtractor2000
{
	public partial class Form1 : Form
	{
		bool isMouseDown = false;
		_3dObjectManager _3DManager;
		bool mode3d = false;
		int spheresFlag = 0;
		bool innerSet, outerSet;
		public Form1()
		{
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;
			KeyPreview = true;
			Canvas.MouseWheel += Canvas_MouseWheel;
			TableLayoutPanel.BackColor = Color.Black;
		}
		private void Canvas_MouseWheel(object sender, MouseEventArgs e)
		{
			if (mode3d)
			{
				int s = _3DManager.WheelScale(e.Delta);
				Canvas.Invalidate();
			}
		}
		private void ImportButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();

			openFileDialog.InitialDirectory = @"C:\Users\MarcinZ\Documents\Visual Studio 2017\Projects\GK1\GK1\3DModels";
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				string path = openFileDialog.FileName;
				string[] p = path.Split('.');
				if (p.Length <= 1 || p[1] != "off") return;

				_3DManager = new _3dObjectManager(path);
				_3DManager.SetCanvasSize(Canvas.Width, Canvas.Height);

				_3DManager.EffectManager.pm = new PhongManager(0.6, 0.2, 0.2, 2, Canvas.Width / 2, Canvas.Height / 2);
				mode3d = true;
				Canvas.Invalidate();
			}
		}

		private void Canvas_MouseDown(object sender, MouseEventArgs e)
		{
			isMouseDown = true;
			if (mode3d)
			{
				_3DManager.UpdateLastPoint(e.Location);
			}
		}
		private void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && mode3d)
			{
				_3DManager.RotateObject(e.Location);
				_3DManager.UpdateLastPoint(e.Location);
				Canvas.Invalidate();
			}
		}
		private void Canvas_Paint(object sender, PaintEventArgs e)
		{
			if (mode3d)
			{
				_3DManager.Paint(Canvas.Width, Canvas.Height, e.Graphics);
			}
		}
		private void Canvas_MouseUp(object sender, MouseEventArgs e)
		{
			Canvas.Invalidate();
			isMouseDown = false;
		}

		private void ZLimitButton_Click(object sender, EventArgs e)
		{
			double ZLimit;
			bool parsed = double.TryParse(ZLimitTextBox.Text, out ZLimit);

			if (!parsed)
			{
				MessageBox.Show("Please enter a valid ZLimit value");
				return;
			}

			if (_3DManager != null)
			{
				if (InnerCheckBox.Checked == false && OuterCheckBox.Checked == false)
				{
					_3DManager.SetObjectZLimit(ZLimit);
					Canvas.Invalidate();
				}
				else
				{
					MessageBox.Show("Turn off both spheres first!");
				}
			}
			else
				MessageBox.Show("Import a model first");
		}

		private void CalculateButton_Click_1(object sender, EventArgs e)
		{
			if (InnerCheckBox.Checked == false && OuterCheckBox.Checked == false)
			{
				if (innerSet && outerSet)
				{
					BackgroundWorker worker = new BackgroundWorker();
					worker.WorkerReportsProgress = true;
					worker.DoWork += worker_DoWork;
					worker.ProgressChanged += worker_ProgressChanged;
					worker.RunWorkerCompleted += work_Done;
					worker.RunWorkerAsync();
				}
				else
				{
					MessageBox.Show("Both shperes must be prepared!");
				}
			}
			else
			{
				MessageBox.Show("Turn off both spheres first!");
			}
		}
		private void worker_DoWork(object sender, DoWorkEventArgs e)
		{
			_3DManager.Calculate(sender);
		}
		private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			ProgressBar.Value = e.ProgressPercentage;
		}
		private void work_Done(object sender, RunWorkerCompletedEventArgs e)
		{
			ProgressBar.Value = 0;
			MessageBox.Show("Done!");
		}

		private void InnerCheckBox_Click(object sender, EventArgs e)
		{
			if (_3DManager != null)
			{
				if (InnerCheckBox.Checked)
				{
					double x, y, z, r;
					bool px = double.TryParse(XTextBox.Text, out x);
					bool py = double.TryParse(YTextBox.Text, out y);
					bool pz = double.TryParse(ZTextBox.Text, out z);
					bool pr = double.TryParse(InnerRTextBox.Text, out r);
					_3DManager.AddInnerSphere(new My3DPoint(px ? x : 0, py ? y : 0, pz ? z : 0), pr ? r : 1);
					spheresFlag = 1;
					innerSet = true;
				}
				else if (spheresFlag == 1 || spheresFlag == 0)
				{
					_3DManager.HideInnerSphere();
					spheresFlag = 0;
				}
				else
				{
					MessageBox.Show("Turn off another sphere first!");
					InnerCheckBox.Checked = true;
				}
				Canvas.Invalidate();
			}
			else
				MessageBox.Show("Import a model first");
		}

		private void OuterCheckBox_Click(object sender, EventArgs e)
		{
			if (_3DManager != null)
			{
				if (OuterCheckBox.Checked)
				{
					double x, y, z, r;
					bool px = double.TryParse(XTextBox.Text, out x);
					bool py = double.TryParse(YTextBox.Text, out y);
					bool pz = double.TryParse(ZTextBox.Text, out z);
					bool pr = double.TryParse(OuterRTextBox.Text, out r);
					_3DManager.AddOuterSphere(new My3DPoint(px ? x : 0, py ? y : 0, pz ? z : 0), pr ? r : 1);
					spheresFlag = 2;
					outerSet = true;
				}
				else if (spheresFlag == 2 || spheresFlag == 0)
				{
					_3DManager.HideOuterSphere();
					spheresFlag = 0;
				}
				else
				{
					MessageBox.Show("Turn off another sphere first!");
					InnerCheckBox.Checked = true;
				}
				Canvas.Invalidate();
			}
			else
				MessageBox.Show("Import a model first");
		}
	}
}
