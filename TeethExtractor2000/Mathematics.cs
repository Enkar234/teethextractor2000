﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeethExtractor2000
{
	static class Mathematics
	{
		public static My3DPoint CrossProduct(My3DPoint v1, My3DPoint v2)
		{
			double s1 = v1.Y * v2.Z - v1.Z * v2.Y;
			double s2 = v1.Z * v2.X - v1.X * v2.Z;
			double s3 = v1.X * v2.Y - v1.Y * v2.X;
			return new My3DPoint(s1, s2, s3);
		}
		public static double DotProduct(My3DPoint v1, My3DPoint v2)
		{
			return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
		}
		public static My3DPoint Difference(My3DPoint v1, My3DPoint v2)
		{
			return new My3DPoint(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
		}
		public static My3DPoint Normalize(My3DPoint V)
		{
			double normV = Math.Sqrt(V.X * V.X + V.Y * V.Y + V.Z * V.Z);
			V = new My3DPoint(V.X / normV, V.Y / normV, V.Z / normV);
			return V;
		}
		public static double Magnitude(My3DPoint v)
		{
			return Math.Sqrt(Math.Pow(v.X, 2) + Math.Pow(v.Y, 2) + Math.Pow(v.Z, 2));
		}
	}
}
