﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeethExtractor2000
{
	class PhongManager
	{
		double Ka;
		double Kd;
		double Ks;
		double alfa = 1;

		List<Point> Lights;
		int vX, vY;
		public PhongManager(double a, double s, double d, double alf, int vx, int vy)
		{
			Ka = a;
			Ks = s;
			Kd = d;
			vX = 0;
			vY = 0;
			alfa = alf;
			Lights = new List<Point>();
			Lights.Add(new Point(vx, vy));
		}
		public (double r, double g, double b) CalculateBrightness(My3DTriangle t, My3DPoint[] points, double d, int factor, double minZ)
		{
			My3DPoint p0 = new My3DPoint(points[t.points[0]].X, points[t.points[0]].Y, points[t.points[0]].Z);
			My3DPoint p1 = new My3DPoint(points[t.points[1]].X, points[t.points[1]].Y, points[t.points[1]].Z);
			My3DPoint p2 = new My3DPoint(points[t.points[2]].X, points[t.points[2]].Y, points[t.points[2]].Z);
			My3DPoint S = new My3DPoint((p0.X + p1.X + p2.X) / 3, (p0.Y + p1.Y + p2.Y) / 3, (p0.Z + p1.Z + p2.Z) / 3);

			My3DPoint N = Mathematics.CrossProduct(Mathematics.Difference(p1, p0), Mathematics.Difference(p2, p0));
			N = Mathematics.Normalize(N);

			My3DPoint V = new My3DPoint(vX - S.X * factor, vY - S.Y * factor, -minZ * factor + S.Z * factor);
			V = Mathematics.Normalize(V);

			double I = Ka;

			foreach (Point l in Lights)
			{
				My3DPoint Lm = new My3DPoint(l.X - S.X * factor, l.Y - S.Y * factor, S.Z * factor);
				Lm = Mathematics.Normalize(Lm);

				double w = 2 * Mathematics.DotProduct(Lm, N);
				My3DPoint Rm = new My3DPoint(w * N.X - Lm.X, w * N.Y - Lm.Y, w * N.Z - Lm.Z);
				Rm = Mathematics.Normalize(Rm);
				double LmN = Mathematics.DotProduct(Lm, N);
				double RmV = Mathematics.DotProduct(Rm, V);

				I += Kd * LmN + Ks * Math.Pow(Math.Abs(RmV), alfa) * Math.Sign(RmV);
			}
			I = I > 1 ? 1 : I < 0 ? 0 : I;

			return (I, I, I);
		}
	}
}
