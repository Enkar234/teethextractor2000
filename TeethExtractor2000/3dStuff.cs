﻿using Extreme.Mathematics;
using Extreme.Mathematics.Curves;
using Extreme.Mathematics.SignalProcessing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Point = System.Drawing.Point;

namespace TeethExtractor2000
{
	class My3DPoint
	{
		public double X { get; set; }
		public double Y { get; set; }
		public double Z { get; set; }
		public My3DPoint(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}
	}
	class My3DTriangle
	{
		public int[] points;
		public My3DTriangle(int p1, int p2, int p3)
		{
			points = new int[3];
			points[0] = p1;
			points[1] = p2;
			points[2] = p3;
		}
	}
	public class MyTriangle
	{
		public Point p1;
		public Point p2;
		public Point p3;
		public Point ul;
		public Point lr;
		Color c;
		public MyTriangle(List<Point> points)
		{
			p1 = points[0];
			p2 = points[1];
			p3 = points[2];
			ul = UpperLeftCorner();
			lr = LowerRightCorner();
		}
		public void SetColor(int v)
		{
			c = Color.FromArgb(v, v, v);
		}
		private Point UpperLeftCorner()
		{
			int x = Math.Min(Math.Min(p1.X, p2.X), p3.X);
			int y = Math.Min(Math.Min(p1.Y, p2.Y), p3.Y);
			return new Point(x, y);
		}
		private Point LowerRightCorner()
		{
			int x = Math.Max(Math.Max(p1.X, p2.X), p3.X);
			int y = Math.Max(Math.Max(p1.Y, p2.Y), p3.Y);
			return new Point(x, y);
		}
		public int[,] GetLines()
		{
			int height = lr.Y - ul.Y + 1;
			int[,] lines = new int[height, 2];
			for (int i = 0; i < height; i++)
			{
				lines[i, 0] = int.MaxValue;
				lines[i, 1] = int.MinValue;
			}
			List<Point> points = new List<Point>(3);
			points.Add(p1);
			points.Add(p2);
			points.Add(p3);

			for (int i = 0; i < 3; i++)
			{
				int startX = points[i].X;
				int startY = points[i].Y;
				int endX = points[(i + 1) % 3].X;
				int endY = points[(i + 1) % 3].Y;
				int dx = Math.Abs(endX - startX);
				int dy = Math.Abs(endY - startY);
				int kx = endX > startX ? 1 : -1;
				int ky = endY > startY ? 1 : -1;
				int d = dx > dy ? 2 * dy - dx : 2 * dx - dy;
				int incrE = dx > dy ? 2 * dy : 2 * dx;
				int incrNE = dx > dy ? 2 * (dy - dx) : 2 * (dx - dy);

				int x = startX;
				int y = startY;

				lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
				lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;

				while (x != endX || y != endY)
				{
					if (d < 0)
					{
						d += incrE;
						if (dx > dy)
						{
							x += kx;
						}
						else
						{
							lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
							lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;
							y += ky;
						}
					}
					else
					{
						int b = Math.Abs(incrE - incrNE);
						d += incrNE;
						lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
						lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;
						x += kx;
						y += ky;
						lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
						lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;
					}
				}
			}
			return lines;
		}
	}
	class My3Dobejct
	{
		List<My3DTriangle> visibleTriangles = new List<My3DTriangle>();
		My3DPoint[] visiblePoints;
		List<My3DPoint> points;
		My3DTriangle[] triangles;
		double minZ, maxZ, minY, maxY, minX, maxX;
		int bmpMinX, bmpMinY, bmpMaxX, bmpMaxY;
		int scale = 1600;
		double S = 1 / Math.Tan(110 * (Math.PI / 180));
		double? ZLimit;
		int index = 0;
		int pointsCount = 0;
		int facesCount = 0;
		int innerPointsStartIndex = 0;
		int innerFacesStartIndex = 0;
		int innerPointsCount = 0;
		int innerFacesCount = 0;
		int outerPointsStartIndex = 0;
		int outerFacesStartIndex = 0;
		int outerPointsCount = 0;
		int outerFacesCount = 0;
		bool innerON = false;
		bool outerON = false;

		My3DPoint sphereCenter;
		double innerR, outerR;
		DirectBitmap innerTeeth2d;
		DirectBitmap outerTeeth2d;
		Color[] Colors;

		public My3Dobejct(string path1, string path2 = null)
		{
			string[] lines1 = System.IO.File.ReadAllLines(path1);

			string p = lines1[1].Split(' ')[0];
			string f = lines1[1].Split(' ')[1];
			int.TryParse(p, out pointsCount);
			int.TryParse(f, out facesCount);
			minZ = double.MaxValue;
			maxZ = double.MinValue;
			minY = double.MaxValue;
			maxY = double.MinValue;
			minX = double.MaxValue;
			maxX = double.MinValue;
			points = new List<My3DPoint>(pointsCount);
			List<My3DTriangle> tri = new List<My3DTriangle>();

			for (int i = 0; i < pointsCount; i++)
			{
				string[] words = lines1[i + 2].Split(' ');
				double x, y, z;
				if (double.TryParse(words[0], NumberStyles.Any, CultureInfo.InvariantCulture, out x) &&
					double.TryParse(words[1], NumberStyles.Any, CultureInfo.InvariantCulture, out y) &&
					double.TryParse(words[2], NumberStyles.Any, CultureInfo.InvariantCulture, out z))
				{
					points.Add(new My3DPoint(x, y, z));
					if (z < minZ) minZ = z;
					if (z > maxZ) maxZ = z;
					if (y < minY) minY = y;
					if (y > maxY) maxY = y;
					if (x < minX) minX = x;
					if (x > maxX) maxX = x;
				}
			}
			for (int i = 0; i < facesCount; i++)
			{
				string[] words = lines1[i + pointsCount + 2].Split(' ');
				int c, p1, p2, p3;
				if (int.TryParse(words[0], out c))
				{
					for (int k = 0; k < c - 2; k++)
					{
						if (int.TryParse(words[1], out p1) && int.TryParse(words[2 + k], out p2) && int.TryParse(words[3 + k], out p3))
						{
							tri.Add(new My3DTriangle(p1, p2, p3));
						}
					}
				}
			}
			double m = maxZ + Math.Abs(minZ) + 1;
			triangles = tri.ToArray();
			visibleTriangles = tri;
			Colors = new Color[visibleTriangles.Count];
			for (int i = 0; i < Colors.Length; i++) Colors[i] = Color.White;
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
		}
		private List<(MyTriangle t, double d, int ix, Color c)> GetPerspectiveView(bool bfc)
		{
			double f = maxZ + Math.Abs(minZ) + 1;
			double n = minZ - maxZ + f;
			double c0 = -f / (f - n);
			double c1 = c0 * n;

			int count = visibleTriangles.Count;
			bmpMaxX = int.MinValue;
			bmpMaxY = int.MinValue;
			bmpMinX = int.MaxValue;
			bmpMinY = int.MaxValue;

			int factor = (int)(scale / ((maxX - minX) + (maxY - minY)));

			List<(MyTriangle t, double d, int ix, Color c)> perspTriangles = new List<(MyTriangle t, double d, int ix, Color c)>(count);
			for (int i = 0; i < count; i++)
			{
				List<Point> newPoints = new List<Point>(3);
				List<My3DPoint> new3DPoints = new List<My3DPoint>(3);
				double sZ = 0;
				for (int v = 0; v < 3; v++)
				{
					sZ += visiblePoints[visibleTriangles[i].points[v]].Z + Math.Abs(minZ) + 1;
					double x = S * visiblePoints[visibleTriangles[i].points[v]].X;
					double y = S * visiblePoints[visibleTriangles[i].points[v]].Y;
					new3DPoints.Add(new My3DPoint(x, y, visiblePoints[visibleTriangles[i].points[v]].Z + Math.Abs(minZ) + 1));
					int newX = (int)(factor * x);
					int newY = (int)(factor * y);
					if (bmpMinX > newX) bmpMinX = newX;
					if (bmpMinY > newY) bmpMinY = newY;
					if (bmpMaxX < newX) bmpMaxX = newX;
					if (bmpMaxY < newY) bmpMaxY = newY;
					newPoints.Add(new Point(newX, newY));
				}
				if (i == facesCount - 1)
				{
					index = perspTriangles.Count;
				}
				if (i < facesCount && bfc && Discard(new3DPoints)) continue;
				double Z = -sZ / 3;
				double d = -c0 - c1 / Z;
				perspTriangles.Add((new MyTriangle(newPoints), d, i, Colors[i]));
			}
			return perspTriangles;
		}
		public DirectBitmap Paint(_3DEffectManager E, int w, int h, bool bfc)
		{
			List<(MyTriangle t, double d, int ix, Color c)> list = GetPerspectiveView(bfc);
			int width = bmpMaxX - bmpMinX + 1 <= w ? bmpMaxX - bmpMinX + 1 : w;
			int height = bmpMaxY - bmpMinY + 1 <= h ? bmpMaxY - bmpMinY + 1 : h;
			int XOffset = bmpMaxX - bmpMinX + 1 <= w ? 0 : (bmpMaxX - bmpMinX + 1) - w;
			int YOffset = bmpMaxY - bmpMinY + 1 <= h ? 0 : (bmpMaxY - bmpMinY + 1) - h;

			DirectBitmap bmp2 = new DirectBitmap(width, height);
			bmp2.bufferZ = new double[width, height];

			for (int i = 0; i < index; i++)
			{
				MyTriangle triangle = list[i].t;
				int[,] lines = triangle.GetLines();

				int y = triangle.ul.Y - bmpMinY - YOffset / 2;

				int yy = 0;
				if (y < 0) yy = Math.Abs(y);

				double v = list[i].d;
				if (v < 0) continue;

				E.phongV = E.pm.CalculateBrightness(visibleTriangles[list[i].ix], visiblePoints, list[i].d, (int)(scale / ((maxX - minX) + (maxY - minY))), minZ);

				for (int j = y + yy; j < y + lines.GetLength(0) && j < height; j++)
				{
					int from = lines[j - y, 0] - bmpMinX - XOffset / 2;
					int to = lines[j - y, 1] - bmpMinX - XOffset / 2;
					if (to < 0) continue;
					from = from < 0 ? 0 : from;
					for (int k = from; k <= to && k < width; k++)
					{
						E.Paint(bmp2, k, j, i, v, list[i].c);
					}
				}
			}
			if (innerON)
			{
				for (int i = index; i < index + innerFacesCount; i++)
				{
					MyTriangle triangle = list[i].t;

					E.GridPaint(bmp2, triangle, points.ToArray(), bmpMinX, bmpMinY, list[i].d, XOffset / 2, YOffset / 2, Color.Blue);
				}
			}
			if (outerON)
			{
				for (int i = index; i < list.Count; i++)
				{
					MyTriangle triangle = list[i].t;

					E.GridPaint(bmp2, triangle, points.ToArray(), bmpMinX, bmpMinY, list[i].d, XOffset / 2, YOffset / 2, Color.Red);
				}
			}
			return bmp2;
		}

		internal void SetZLimit(double zLimit)
		{
			this.ZLimit = zLimit;
			List<My3DTriangle> newTriangles = new List<My3DTriangle>();
			foreach (My3DTriangle t in triangles)
			{
				if (points[t.points[0]].Z < ZLimit.Value || points[t.points[1]].Z < ZLimit.Value || points[t.points[2]].Z < ZLimit.Value)
					continue;
				newTriangles.Add(t);
			}
			visibleTriangles = newTriangles;
			facesCount = visibleTriangles.Count;
		}
		public void AddInnerSphere(My3DPoint center, double R)
		{
			innerON = true;
			this.sphereCenter = center;
			this.innerR = R;

			innerPointsStartIndex = points.Count;
			innerFacesStartIndex = visibleTriangles.Count;
			string path = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName + "\\Sphere2.off";
			string[] lines = File.ReadAllLines(path);

			string p = lines[1].Split(' ')[0];
			string f = lines[1].Split(' ')[1];
			int.TryParse(p, out innerPointsCount);
			int.TryParse(f, out innerFacesCount);

			List<My3DTriangle> tri = new List<My3DTriangle>();
			(My3DPoint sphereCenter, double r) = CenterAndR(lines, innerPointsCount);

			for (int i = 0; i < innerPointsCount; i++)
			{
				string[] words = lines[i + 2].Split(' ');
				double x, y, z;
				if (double.TryParse(words[0], NumberStyles.Any, CultureInfo.InvariantCulture, out x) &&
					double.TryParse(words[1], NumberStyles.Any, CultureInfo.InvariantCulture, out y) &&
					double.TryParse(words[2], NumberStyles.Any, CultureInfo.InvariantCulture, out z))
				{
					x += (center.X - sphereCenter.X);
					y += (center.Y - sphereCenter.Y);
					z += (center.Z - sphereCenter.Z);

					x += (center.X - x) * ((r - R) / r);
					y += (center.Y - y) * ((r - R) / r);
					z += (center.Z - z) * ((r - R) / r);

					points.Add(new My3DPoint(x, y, z));
					if (z < minZ) minZ = z;
					if (z > maxZ) maxZ = z;
					if (y < minY) minY = y;
					if (y > maxY) maxY = y;
					if (x < minX) minX = x;
					if (x > maxX) maxX = x;
				}
			}
			for (int i = 0; i < innerFacesCount; i++)
			{
				string[] words = lines[i + innerPointsCount + 2].Split(' ');
				int c, p1, p2, p3;
				if (int.TryParse(words[0], out c))
				{
					for (int k = 0; k < c - 2; k++)
					{
						if (int.TryParse(words[1], out p1) && int.TryParse(words[2 + k], out p2) && int.TryParse(words[3 + k], out p3))
						{
							tri.Add(new My3DTriangle(p1 + innerPointsStartIndex, p2 + innerPointsStartIndex, p3 + innerPointsStartIndex));
						}
					}
				}
			}
			double m = maxZ + Math.Abs(minZ) + 1;
			visibleTriangles.AddRange(tri);
			Colors = new Color[visibleTriangles.Count];
			for (int i = 0; i < Colors.Length; i++) Colors[i] = Color.White;
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
		}
		public void AddOuterSphere(My3DPoint center, double R)
		{
			outerON = true;
			this.sphereCenter = center;
			this.outerR = R;

			outerPointsStartIndex = points.Count;
			outerFacesStartIndex = visibleTriangles.Count;
			string path = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName + "\\Sphere2.off";
			string[] lines = File.ReadAllLines(path);

			string p = lines[1].Split(' ')[0];
			string f = lines[1].Split(' ')[1];
			int.TryParse(p, out outerPointsCount);
			int.TryParse(f, out outerFacesCount);

			List<My3DTriangle> tri = new List<My3DTriangle>();
			(My3DPoint sphereCenter, double r) = CenterAndR(lines, outerPointsCount);

			for (int i = 0; i < outerPointsCount; i++)
			{
				string[] words = lines[i + 2].Split(' ');
				double x, y, z;
				if (double.TryParse(words[0], NumberStyles.Any, CultureInfo.InvariantCulture, out x) &&
					double.TryParse(words[1], NumberStyles.Any, CultureInfo.InvariantCulture, out y) &&
					double.TryParse(words[2], NumberStyles.Any, CultureInfo.InvariantCulture, out z))
				{
					x += (center.X - sphereCenter.X);
					y += (center.Y - sphereCenter.Y);
					z += (center.Z - sphereCenter.Z);

					x += (center.X - x) * ((r - R) / r);
					y += (center.Y - y) * ((r - R) / r);
					z += (center.Z - z) * ((r - R) / r);

					points.Add(new My3DPoint(x, y, z));
					if (z < minZ) minZ = z;
					if (z > maxZ) maxZ = z;
					if (y < minY) minY = y;
					if (y > maxY) maxY = y;
					if (x < minX) minX = x;
					if (x > maxX) maxX = x;
				}
			}
			for (int i = 0; i < outerFacesCount; i++)
			{
				string[] words = lines[i + outerPointsCount + 2].Split(' ');
				int c, p1, p2, p3;
				if (int.TryParse(words[0], out c))
				{
					for (int k = 0; k < c - 2; k++)
					{
						if (int.TryParse(words[1], out p1) && int.TryParse(words[2 + k], out p2) && int.TryParse(words[3 + k], out p3))
						{
							tri.Add(new My3DTriangle(p1 + outerPointsStartIndex, p2 + outerPointsStartIndex, p3 + outerPointsStartIndex));
						}
					}
				}
			}
			double m = maxZ + Math.Abs(minZ) + 1;
			visibleTriangles.AddRange(tri);
			Colors = new Color[visibleTriangles.Count];
			for (int i = 0; i < Colors.Length; i++) Colors[i] = Color.White;
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
		}
		public void HideInnerSphere()
		{
			innerON = false;
			points.RemoveRange(innerPointsStartIndex, innerPointsCount);
			visibleTriangles.RemoveRange(innerFacesStartIndex, innerFacesCount);
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
		}
		public void HideOuterSphere()
		{
			outerON = false;
			points.RemoveRange(outerPointsStartIndex, outerPointsCount);
			visibleTriangles.RemoveRange(outerFacesStartIndex, outerFacesCount);
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
		}
		private (My3DPoint center, double R) CenterAndR(string[] lines, int n)
		{
			double Xmin = double.MaxValue;
			double Xmax = double.MinValue;
			double Ymin = double.MaxValue;
			double Ymax = double.MinValue;
			double Zmin = double.MaxValue;
			double Zmax = double.MinValue;
			for (int i = 0; i < n; i++)
			{
				string[] words = lines[i + 2].Split(' ');
				if (double.TryParse(words[0], NumberStyles.Any, CultureInfo.InvariantCulture, out double x) &&
					double.TryParse(words[1], NumberStyles.Any, CultureInfo.InvariantCulture, out double y) &&
					double.TryParse(words[2], NumberStyles.Any, CultureInfo.InvariantCulture, out double z))
				{
					if (z < Zmin) Zmin = z;
					if (z > Zmax) Zmax = z;
					if (y < Ymin) Ymin = y;
					if (y > Ymax) Ymax = y;
					if (x < Xmin) Xmin = x;
					if (x > Xmax) Xmax = x;
				}
			}
			return (new My3DPoint((Xmin + Xmax) / 2, (Ymin + Ymax) / 2, (Zmin + Zmax) / 2), Math.Abs(Xmin - Xmax) / 2);
		}

		public void XRotate(double fi)
		{
			double[,] X = GetXMatrix(fi);
			Rotate(X);
		}
		public void YRotate(double fi)
		{
			double[,] Y = GetYMatrix(fi);
			Rotate(Y);
		}
		public void ZRotate(double fi)
		{
			double[,] Z = GetZMatrix(fi);
			Rotate(Z);
		}
		private void Rotate(double[,] Matrix)
		{
			minZ = double.MaxValue;
			maxZ = double.MinValue;
			Parallel.For(0, visiblePoints.Length, i =>
			{
				double[] newVector = new double[3];
				double[] oldVector = new double[3];
				oldVector[0] = visiblePoints[i].X;
				oldVector[1] = visiblePoints[i].Y;
				oldVector[2] = visiblePoints[i].Z;

				for (int j = 0; j < 3; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						newVector[j] += Matrix[j, k] * oldVector[k];
					}
				}
				double x = newVector[0];
				double y = newVector[1];
				double z = newVector[2];
				if (z < minZ) minZ = z;
				if (z > maxZ) maxZ = z;

				visiblePoints[i].X = x;
				visiblePoints[i].Y = y;
				visiblePoints[i].Z = z;
			});
		}
		public void Scale(float s)
		{
			scale = (int)(s * 1600);
		}
		private bool Discard(List<My3DPoint> points)
		{
			My3DPoint N = Mathematics.CrossProduct(Mathematics.Difference(points[1], points[0]), Mathematics.Difference(points[2], points[0]));
			return N.Z <= 0;
		}
		private double[,] GetRotationMatrix(double fi, My3DPoint u)
		{
			double[,] M = { { Math.Cos(fi)+u.X*u.X*(1-Math.Cos(fi)),u.X*u.Y*(1-Math.Cos(fi))-u.Z*Math.Sin(fi),u.X*u.Z*(1-Math.Cos(fi))+u.Y*Math.Sin(fi)},
							{ u.X*u.Y*(1-Math.Cos(fi))+u.Z*Math.Sin(fi),Math.Cos(fi)+u.Y*u.Y*(1-Math.Cos(fi)),u.Y*u.Z*(1-Math.Cos(fi))-u.X*Math.Sin(fi)},
							{ u.Z*u.X*(1-Math.Cos(fi))-u.Y*Math.Sin(fi),u.Z*u.Y*(1-Math.Cos(fi))+u.X*Math.Sin(fi), Math.Cos(fi)+u.Z*u.Z*(1-Math.Cos(fi))}};
			return M;
		}
		private double[,] GetXMatrix(double fi)
		{
			return new double[,] { { 1, 0, 0 }, { 0, Math.Cos(fi), -Math.Sin(fi) }, { 0, Math.Sin(fi), Math.Cos(fi) } };
		}
		private double[,] GetYMatrix(double fi)
		{
			return new double[,] { { Math.Cos(fi), 0, Math.Sin(fi) }, { 0, 1, 0 }, { -Math.Sin(fi), 0, Math.Cos(fi) } };
		}
		private double[,] GetZMatrix(double fi)
		{
			return new double[,] { { Math.Cos(fi), -Math.Sin(fi), 0 }, { Math.Sin(fi), Math.Cos(fi), 0 }, { 0, 0, 1 } };
		}
		public double CenterZ()
		{
			return ((minZ + 2 * Math.Abs(minZ) + 2) / 2) * 1 / Math.Tan(110 * (Math.PI / 180)) * (int)(scale / ((maxX - minX) + (maxY - minY)));
		}

		public void Calculate(object sender)
		{
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
			int scalingFactor = 2;
			List<List<(int x, int y, int c)>> inner2DTriangles = new List<List<(int x, int y, int c)>>();
			List<List<(int x, int y, int c)>> outer2DTriangles = new List<List<(int x, int y, int c)>>();

			DirectBitmap innerProjecton = new DirectBitmap(360 * scalingFactor, 180 * scalingFactor);
			DirectBitmap outerProjecton = new DirectBitmap(360 * scalingFactor, 180 * scalingFactor);

			foreach (My3DTriangle triangle in visibleTriangles)
			{
				int[] pointIndexes = triangle.points;
				List<My3DPoint> points = new List<My3DPoint>(3);
				for (int i = 0; i < 3; i++)
				{
					points.Add(visiblePoints[pointIndexes[i]]);
				}
				My3DPoint N = Mathematics.CrossProduct(Mathematics.Difference(points[1], points[0]), Mathematics.Difference(points[2], points[0])); //triangle's normal vector
				List<My3DPoint> Rs = new List<My3DPoint>(3);
				for (int i = 0; i < 3; i++)
				{
					Rs.Add(Mathematics.Difference(sphereCenter, points[i]));
				}
				double angle = Math.Acos(Mathematics.DotProduct(N, Rs[0]) / (Mathematics.Magnitude(N) * Mathematics.Magnitude(Rs[0])));

				if (angle > Math.PI / 2) //outer sphere
				{
					outer2DTriangles.Add(new List<(int x, int y, int c)>());
					for (int i = 0; i < 3; i++)
					{
						int intensity = (int)(((outerR - Mathematics.Magnitude(Rs[i])) / outerR) * 255 * f(-angle + Math.PI));
						intensity = intensity > 255 ? 255 : intensity < 0 ? 0 : intensity;
						double theta = Math.Acos(points[i].Z / outerR) * (180 / Math.PI);
						double phi = Math.Atan2(points[i].Y, points[i].X) * (180 / Math.PI);
						if (phi < 0) phi += 360;
						phi += 270;
						phi = phi % 360;
						outer2DTriangles[outer2DTriangles.Count - 1].Add(((int)(phi * scalingFactor), (int)(theta * scalingFactor), intensity));
					}
				}
				else //inner sphere
				{
					inner2DTriangles.Add(new List<(int x, int y, int c)>());
					for (int i = 0; i < 3; i++)
					{
						int intensity = (int)(((Mathematics.Magnitude(Rs[i]) - innerR) / outerR) * 255 * f(angle));
						intensity = intensity > 255 ? 255 : intensity < 0 ? 0 : intensity;
						double theta = Math.Acos(points[i].Z / innerR) * (180 / Math.PI);
						double phi = Math.Atan2(points[i].Y, points[i].X) * (180 / Math.PI);
						if (phi < 0) phi += 360;
						phi += 270;
						phi = phi % 360;
						inner2DTriangles[inner2DTriangles.Count - 1].Add(((int)(phi * scalingFactor), (int)(theta * scalingFactor), intensity));
					}
				}

				double f(double x)
				{
					double d = 3;
					double from = Math.PI / 4 - d;
					double to = Math.PI / 4 + d;
					double k = (x * (to - from) * 2) / Math.PI + from;
					return -Math.Atan(k - Math.PI / 4) / (2 * Math.Atan(Math.PI / 4)) + 0.5;
				}
			}
			DrawTeeth(innerProjecton, inner2DTriangles, "inner");
			(sender as BackgroundWorker).ReportProgress(50);
			DrawTeeth(outerProjecton, outer2DTriangles, "outer");
			visiblePoints = points.Select(point => new My3DPoint(point.X, point.Y, point.Z)).ToArray();
			CalculateBack();
			(sender as BackgroundWorker).ReportProgress(100);
		}
		private void DrawTeeth(DirectBitmap bmp, List<List<(int x, int y, int c)>> triangleList, string name)
		{
			foreach (var tr in triangleList)
			{
				List<(Point p, int c)> trPoints = new List<(Point p, int c)>(3);
				int centerX = 0;
				int centerY = 0;
				foreach (var p in tr)
				{
					trPoints.Add((new Point(p.x, p.y), p.c));
					centerX += p.x;
					centerY += p.y;
				}
				List<(MyTriangle miniTr, Color c)> triangles = new List<(MyTriangle miniTr, Color c)>(3);
				Point center = new Point(centerX / 3, centerY / 3);
				for (int i = 0; i < 3; i++)
				{
					List<Point> trianglePoints = new List<Point>(3);
					int vc = (trPoints[i % 3].c + trPoints[(i + 1) % 3].c) / 2;
					Color color = Color.FromArgb(vc, vc, vc);
					trianglePoints.Add(trPoints[i % 3].p);
					trianglePoints.Add(trPoints[(i + 1) % 3].p);
					trianglePoints.Add(center);
					triangles.Add((new MyTriangle(trianglePoints), color));
				}
				foreach ((MyTriangle miniTr, Color c) in triangles)
				{
					bmp.DrawTriangle(miniTr, c);
				}
			}
			//bmp.Bitmap.Save($"original_{name}.png", ImageFormat.Png);
			bmp.Equalize();
			//bmp.Bitmap.Save($"equalized_{name}.png", ImageFormat.Png);
			bmp.GaussianFilter(3, 3, 2.7f);
			//bmp.Bitmap.Save($"gaussian_Filter_{name}.png", ImageFormat.Png);
			bmp.ReduceColors(3);
			//bmp.Bitmap.Save($"reduced_colors_{name}.png", ImageFormat.Png);
			bmp.FindBestEdge(name);
			//bmp.Bitmap.Save($"found_teeth_{name}.png", ImageFormat.Png);
			bmp.FillTeeth(name);
			//bmp.Bitmap.Save($"filled_{name}.png", ImageFormat.Png);
			if (name == "inner") innerTeeth2d = bmp;
			else outerTeeth2d = bmp;
		}
		private void CalculateBack()
		{
			int scalingFactor = 2;
			Colors = new Color[visibleTriangles.Count];

			for (int k = 0; k < visibleTriangles.Count; k++)
			{
				int[] pointIndexes = visibleTriangles[k].points;
				List<My3DPoint> points = new List<My3DPoint>(3);
				for (int i = 0; i < 3; i++)
				{
					points.Add(visiblePoints[pointIndexes[i]]);
				}
				My3DPoint N = Mathematics.CrossProduct(Mathematics.Difference(points[1], points[0]), Mathematics.Difference(points[2], points[0])); //triangle's normal vector
				List<My3DPoint> Rs = new List<My3DPoint>(3);
				for (int i = 0; i < 3; i++)
				{
					Rs.Add(Mathematics.Difference(sphereCenter, points[i]));
				}
				double angle = Math.Acos(Mathematics.DotProduct(N, Rs[0]) / (Mathematics.Magnitude(N) * Mathematics.Magnitude(Rs[0])));

				if (angle > Math.PI / 2) //outer sphere
				{
					for (int i = 0; i < 3; i++)
					{
						double theta = Math.Acos(points[i].Z / outerR) * (180 / Math.PI);
						double phi = Math.Atan2(points[i].Y, points[i].X) * (180 / Math.PI);
						if (phi < 0) phi += 360;
						phi += 270;
						phi = phi % 360;
						Colors[k] = outerTeeth2d.GetPixel((int)(phi * scalingFactor), (int)(theta * scalingFactor));
					}
				}
				else //inner sphere
				{
					for (int i = 0; i < 3; i++)
					{
						double theta = Math.Acos(points[i].Z / innerR) * (180 / Math.PI);
						double phi = Math.Atan2(points[i].Y, points[i].X) * (180 / Math.PI);
						if (phi < 0) phi += 360;
						phi += 270;
						phi = phi % 360;
						Colors[k] = innerTeeth2d.GetPixel((int)(phi * scalingFactor), (int)(theta * scalingFactor));
					}
				}
			}
		}
	}
}
