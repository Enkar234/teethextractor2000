﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeethExtractor2000
{
	class Graph
	{
		private int vertexCount;
		private int edgesCount;
		private Vertex[] points;
		private List<VertexAdjacencyList> adjacencyList;
		private int Width;
		private int Height;

		public Graph(int width, int height, int[,] points, bool left, bool right, bool down=true)
		{
			Width = width;
			Height = height;

			int w = Width;
			int n = Width * Height;

			List<Vertex> graphPoints = new List<Vertex>();
			List<Edge> edges = new List<Edge>();
			for (int i = 0; i < n; i++)
			{
				graphPoints.Add(new Vertex(i % w, i / w, i));
			}
			for (int i = 0; i < Height - 1; i++)
			{
				for (int j = 0; j < Width - 1; j++) //in goal area horizontal
				{
					int weight = points[j, i] <= 10 ? 500 : -points[j, i];
					if (left)
					{
						edges.Add(new Edge(w * i + j + 1, w * i + j, weight)); //left
					}
					if(right)
					{
						edges.Add(new Edge(w * i + j, w * i + j + 1, weight)); //right
					}
					if (down)
					{
						edges.Add(new Edge(w * i + j, w * (i + 1) + j, weight)); //down
					}
					edges.Add(new Edge(w * (i + 1) + j, w * i + j, weight)); //up
				}
			}

			vertexCount = graphPoints.Count;
			edgesCount = edges.Count;
			this.points = graphPoints.ToArray();
			adjacencyList = new List<VertexAdjacencyList>();

			for (int i = 0; i < vertexCount; i++)
			{
				adjacencyList.Add(new VertexAdjacencyList(i));
			}

			foreach (Edge e in edges)
			{
				adjacencyList[e.From].OutEdges.Add(e.Clone());
			}
		}

		public List<(int x, int y)> AStarShortestPath(int fromX, int fromY, int toX, int toY)
		{
			int from = fromY * Width + fromX;
			int to = toY * Width + toX;

			List<VertexDistance> open = new List<VertexDistance>();
			bool[] closed = new bool[vertexCount];
			double[] terminalDistance = GetDistanceHeuristics(to);
			double[] sourceDistance = new double[vertexCount];
			int[] previous = new int[vertexCount];
			for (int i = 0; i < vertexCount; i++)
			{
				previous[i] = int.MinValue;
			}

			open.Add(new VertexDistance(from, terminalDistance[from]));

			while (open.Count > 0)
			{
				open.Sort((x, y) => (int)(y.Distance - x.Distance));
				VertexDistance bestVertexDistance = open[open.Count - 1];
				int v = bestVertexDistance.Vertex;

				open.RemoveAt(open.Count - 1);
				closed[bestVertexDistance.Vertex] = true;

				if (v == to)
					break;

				foreach (Edge e in adjacencyList[v].OutEdges)
				{
					if (closed[e.To])
						continue;

					if (open.Exists((x => x.Vertex == e.To)) == false)
					{
						sourceDistance[e.To] = double.MaxValue;
						open.Add(new VertexDistance(e.To, sourceDistance[e.To] + terminalDistance[e.To]));
					}
					if (sourceDistance[e.To] > sourceDistance[e.From] + e.Weight)
					{
						sourceDistance[e.To] = sourceDistance[e.From] + e.Weight;
						previous[e.To] = e.From;

						VertexDistance vertexDistance = open.Find((x => x.Vertex == e.To));
						vertexDistance.Distance = sourceDistance[e.To] + terminalDistance[e.To];
					}
				}

			}

			List<int> indexedPath = GetPath(from, to, previous);
			if (indexedPath == null)
			{
				return null;
			}

			List<(int x, int y)> resultPath = new List<(int x, int y)>();
			foreach (int vertex in indexedPath)
			{
				int x = points[vertex].X;
				int y = points[vertex].Y;
				resultPath.Add((x, y));
			}
			return resultPath;
		}

		private List<int> GetPath(int from, int to, int[] previous)
		{
			List<int> path = new List<int>();
			int currentVertex = to;

			while (currentVertex != from)
			{
				path.Add(currentVertex);
				currentVertex = previous[currentVertex];
				if (currentVertex == int.MinValue)
				{
					return null;
				}
			}
			path.Add(currentVertex);
			path.Reverse();

			return path;
		}

		private double[] GetDistanceHeuristics(int from)
		{
			double[] distance = new double[vertexCount];

			for (int i = 0; i < vertexCount; i++)
			{
				if (i == from)
					distance[i] = 0;
				else
					distance[i] = Math.Abs(points[from].X - points[i].X) + Math.Abs(points[from].Y - points[i].Y);
			}

			return distance;
		}

		public void DeleteEdge(int fromX, int fromY, int toX, int toY)
		{
			int from = fromY * Width + fromX;
			int to = toY * Width + toX;

			foreach (VertexAdjacencyList list in adjacencyList)
			{
				for (int i = 0; i < list.OutEdges.Count; i++)
				{
					if (list.OutEdges[i].From == from && list.OutEdges[i].To == to)
					{
						list.OutEdges.RemoveAt(i);
						i--;
					}
					else if (list.OutEdges[i].From == to && list.OutEdges[i].To == from)
					{
						list.OutEdges.RemoveAt(i);
						i--;
					}
				}
			}
		}
	}

	internal class VertexDistance
	{
		public int Vertex;
		public double Distance;

		public VertexDistance(int v, double dist)
		{
			Vertex = v;
			Distance = dist;
		}
	}

	internal class VertexAdjacencyList
	{
		public int Vertex;
		public List<Edge> OutEdges;

		public VertexAdjacencyList(int vertex)
		{
			Vertex = vertex;
			OutEdges = new List<Edge>();
		}
	}

	internal class Edge
	{
		public int From;
		public int To;
		public double Weight;

		public Edge(int from, int to, double w)
		{
			From = from;
			To = to;
			Weight = w;
		}

		public Edge Clone()
		{
			return new Edge(From, To, Weight);
		}
	}

	internal struct Vertex
	{
		public int X { get; }
		public int Y { get; }
		public int Index { get; }
		public Vertex(int x, int y, int index)
		{
			X = x;
			Y = y;
			Index = index;
		}
	}
}
