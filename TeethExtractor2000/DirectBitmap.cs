﻿using Extreme.Mathematics;
using Extreme.Mathematics.SignalProcessing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;

namespace TeethExtractor2000
{
	public class DirectBitmap : IDisposable
	{
		public Bitmap Bitmap { get; private set; }
		public Int32[] Bits { get; private set; }
		public bool Disposed { get; private set; }
		public int Height { get; private set; }
		public int Width { get; private set; }
		public double[,] bufferZ;
		private bool[] setPixels;
		protected GCHandle BitsHandle { get; private set; }

		public DirectBitmap(int width, int height)
		{
			Width = width;
			Height = height;
			Bits = new Int32[width * height];
			setPixels = new bool[width * height];
			BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
			Bitmap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());
		}
		public DirectBitmap(DirectBitmap dbmp)
		{
			Width = dbmp.Width;
			Height = dbmp.Height;
			Bits = new Int32[Width * Height];
			dbmp.Bits.CopyTo(Bits, 0);
			BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
			Bitmap = new Bitmap(Width, Height, Width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());
		}
		public void SetPixel(int x, int y, Color color)
		{
			int index = x + (y * Width);
			int col = color.ToArgb();
			setPixels[index] = true;
			Bits[index] = col;
		}
		public Color GetPixel(int x, int y)
		{
			int index = x + (y * Width);
			int col = Bits[index];
			Color result = Color.FromArgb(col);

			return result;
		}
		public void SetVPixel(int x, int y, Color c, double v)
		{
			if (v > bufferZ[x, y])
			{
				bufferZ[x, y] = v;
				SetPixel(x, y, c);
			}
		}
		public void DrawLine(Point from, Point to, Color c, double v = 0)
		{
			int dx = Math.Abs(to.X - from.X);
			int dy = Math.Abs(to.Y - from.Y);
			int kx = to.X > from.X ? 1 : -1;
			int ky = to.Y > from.Y ? 1 : -1;
			int d = dx > dy ? 2 * dy - dx : 2 * dx - dy;
			int incrE = dx > dy ? 2 * dy : 2 * dx;
			int incrNE = dx > dy ? 2 * (dy - dx) : 2 * (dx - dy);

			int x = from.X;
			int y = from.Y;
			if (!Out(x, y) && v > bufferZ[x, y])
			{
				bufferZ[x, y] = v;
				SetPixel(x, y, c);
			}
			while (x != to.X || y != to.Y)
			{
				if (d < 0)
				{
					d += incrE;
					if (dx > dy) x += kx;
					else y += ky;
				}
				else
				{
					int b = Math.Abs(incrE - incrNE);
					if (!Out(x, y) && v > bufferZ[x, y])
					{
						bufferZ[x, y] = v;
						SetPixel(x, y, c);
					}
					d += incrNE;
					x += kx;
					y += ky;
				}
				if (!Out(x, y) && v > bufferZ[x, y])
				{
					bufferZ[x, y] = v;
					SetPixel(x, y, c);
				}
			}
			bool Out(int xx, int yy)
			{
				return xx >= Width || yy >= Height || xx < 0 || yy < 0;
			}
		}
		public void DrawTriangle(MyTriangle triangle, Color c)
		{
			int[,] lines = triangle.GetLines();
			int yFrom = triangle.ul.Y;
			for (int j = 0; j < lines.GetLength(0); j++)
			{
				int from = lines[j, 0];
				int to = lines[j, 1];
				for (int k = from; k <= to; k++)
				{
					SetPixel(k, yFrom + j, c);
				}
			}
		}

		public void Equalize()
		{
			int[] lum = new int[256];
			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					int c = GetPixel(i, j).R;
					lum[c]++;
				}
			}
			int[] cdf = CDF(lum);
			double min = cdf.Min();
			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					int a = GetPixel(i, j).R;
					int c = (int)(((cdf[a] - min) / (Width * Height - min)) * 255);
					int index = i + (j * Width);
					if (setPixels[index])
					{
						SetPixel(i, j, Color.FromArgb(c, c, c));
					}
				}
			}
		}
		private int[] CDF(int[] values)
		{
			int[] cdf = new int[256];
			int n = Width * Height;
			cdf[0] = values[0];
			for (int i = 1; i < 256; i++)
			{
				cdf[i] = cdf[i - 1] + values[i];
			}
			return cdf;
		}
		public void GaussianFilter(int r, int c, float weight)
		{
			int lr = r / 2;
			int kr = r % 2 == 0 ? lr : lr + 1;
			int lc = c / 2;
			int kc = c % 2 == 0 ? lc : lc + 1;
			double[,] Matrix = new double[c, r];
			double sum = 0;
			for (int i = 0; i < c; i++)
			{
				for (int j = 0; j < r; j++)
				{
					Matrix[i, j] = (1 / (2 * Math.PI * Math.Pow(weight, 2))) * Math.Exp(-((Math.Pow(((r - 1) / 2) - i, 2) + Math.Pow(((c - 1) / 2) - j, 2)) / (2 * Math.Pow(weight, 2))));
					sum += Matrix[i, j];
				}
			}
			for (int i = 0; i < c; i++)
			{
				for (int j = 0; j < r; j++)
				{
					Matrix[i, j] *= (1.0 / sum);
				}
			}
			for (int i = lc; i <= Width - kc; i++)
			{
				for (int j = lr; j <= Height - kr; j++)
				{
					double sumI = 0;
					for (int x = i - lc; x < i + kc; x++)
					{
						for (int y = j - lr; y < j + kr; y++)
						{
							sumI += GetPixel(x, y).R * Matrix[x - i + lc, y - j + lr];
						}
					}
					int C = (int)sumI;
					SetPixel(i, j, Color.FromArgb(C, C, C));
				}
			}
		}
		public void ReduceColors(int k)
		{
			Octree tree = OctreeBuilder.PrepareOctree(this);
			tree = OctreeBuilder.ReduceToK(tree, k);

			for (int x = 0; x < Width; x++)
			{
				for (int y = 0; y < Height; y++)
				{
					Color c = GetPixel(x, y);
					Color rc = OctreeBuilder.ReducedColor(c, tree, 7);
					SetPixel(x, y, rc);
				}
			}
		}
		public void FindBestEdge(string name)
		{
			int[,] points = new int[Width, Height];
			int[] tab = new int[9] { 2, 1, 0, -1, -4, -1, 0, 1, 2 };
			for (int x = 4; x < Width - 5; x++)
			{
				for (int y = Height - 5; y >= 4; y--)
				{
					int sum = 0;
					for (int i = -4; i < 5; i++)
					{
						int pvx = GetPixel(x, y + i).R;
						int pvy = GetPixel(x + i, y).R;
						if (pvx == 0 || pvy == 0)
						{
							sum = 0;
							break;
						}
						sum += pvx * tab[i + 4] + pvy * tab[i + 4];
					}
					points[x, y] = sum;
					int c = sum > 255 ? 255 : sum < 0 ? 0 : sum;
				}
			}
			int max = int.MinValue;
			int maxX = 0;
			int maxY = 0;

			for (int x = Width / 3; x < 2 * Width / 3; x++)
			{
				for (int y = Height - 5; y >= 4; y--)
				{
					if (points[x, y] > max)
					{
						max = points[x, y];
						maxX = x;
						maxY = y;
					}
				}
			}
			Graph leftGraph = new Graph(Width, Height, points, true, false);
			Graph rightGraph = new Graph(Width, Height, points, false, true);
			List<(int x, int y)> leftPoints = leftGraph.AStarShortestPath(maxX, maxY, 0, maxY);
			List<(int x, int y)> rightPoints = rightGraph.AStarShortestPath(maxX, maxY, Width - 1, maxY);
			List<(int x, int y)> allPoints = new List<(int x, int y)>(leftPoints.Count + rightPoints.Count);
			leftPoints.Reverse();
			allPoints.AddRange(leftPoints);
			allPoints.AddRange(rightPoints);

			double[] badsignal = new double[allPoints.Count];
			int j = 0;
			while (j < allPoints.Count)
			{
				int sumY = 0;
				int x = allPoints[j].x;
				int k = 0;
				while (j < allPoints.Count && allPoints[j].x == x)
				{
					sumY += allPoints[j].y;
					k++;
					j++;
				}
				badsignal[x] = sumY / k;
			}
			var signal = Vector.Create(badsignal);
			var smoothed = Smoothing.MovingAverage(badsignal, 7);
			allPoints.Clear();
			int l = 0;
			while (l < smoothed.Length && smoothed[l] > 0)
			{
				int y = smoothed[l] - (int)smoothed[l] > 0.5 ? (int)smoothed[l] + 1 : (int)smoothed[l];
				allPoints.Add((l, y));
				l++;
			}
			foreach ((int x, int y) in allPoints)
			{
				SetPixel(x, y, Color.Blue);
			}

			allPoints.Select(x => x.x < Width);
			FillBrightestTeeth();
			//Bitmap.Save($"FillBrightestTeeth_{name}.png", ImageFormat.Png);
			FindSpaces(allPoints, name);
		}
		private void FindSpaces(List<(int x, int y)> bluePoints, string name)
		{
			int[,] points = new int[Width, Height];
			int[] tab = new int[9] { 2, 1, 0, -1, -4, -1, 0, 1, 2 };
			for (int x = 4; x < Width - 5; x++)
			{
				for (int y = Height - 5; y >= 4; y--)
				{
					int sum = 0;
					for (int i = -4; i < 5; i++)
					{
						int pvy = GetPixel(x + i, y).R;
						sum += pvy * tab[i + 4];
					}
					int c = sum > 255 ? 255 : sum < 0 ? 0 : sum;
					points[x, y] = sum;
				}
			}

			int max = int.MinValue;
			int maxX = 0;
			int maxY = 0;

			for (int x = Width / 3; x < 2 * Width / 3; x++)
			{
				for (int y = Height - 5; y >= 4; y--)
				{
					if (points[x, y] > max)
					{
						max = points[x, y];
						maxX = x;
						maxY = y;
					}
				}
			}

			foreach ((int x, int y) in bluePoints)
			{
				if (x >= Width) continue;
				int[,] miniPoints = new int[60, 100];
				for (int i = y; i > y - 70 && i > 0; i--)
				{
					for (int j = x; j < x + 30 && j < Width; j++)
					{
						miniPoints[30 + (j - x), 69 - (y - i)] = points[j, i];
					}
					for (int j = x - 1; j >= x - 30 && j >= 0; j--)
					{
						miniPoints[30 - (x - j), 69 - (y - i)] = points[j, i];
					}
				}
				for (int i = y; i < y + 30 && i < Height; i++)
				{
					for (int j = x; j < x + 30 && j < Width; j++)
					{
						miniPoints[30 + (j - x), 70 + (i - y)] = points[j, i];
					}
					for (int j = x - 1; j >= x - 30 && j >= 0; j--)
					{
						miniPoints[30 - (x - j), 70 + (i - y)] = points[j, i];
					}
				}

				Graph g = new Graph(60, 100, miniPoints, true, true, false);
				List<(int x, int y)> path = g.AStarShortestPath(30, 70, 30, 0);

				List<double> badsignal = new List<double>();
				int jj = 0;
				while (path[jj].y > 0)
				{
					int sumX = 0;
					int yy = path[jj].y;
					int k = 0;
					while (jj < path.Count && path[jj].y == yy)
					{
						sumX += path[jj].x;
						k++;
						jj++;
					}
					badsignal.Add(sumX / k);
				}
				var signal = Vector.Create(badsignal);
				var smoothed = Smoothing.MovingAverage(badsignal.ToArray(), 11);
				path.Clear();
				for (int i = smoothed.Count - 1; i >= 0; i--)
				{
					int yy = smoothed[i] - (int)smoothed[i] > 0.5 ? (int)smoothed[i] + 1 : (int)smoothed[i];
					path.Add((yy, 70 - i));
				}

				foreach ((int xx, int yy) in path)
				{
					SetPixel(x + xx - 30, y + yy - 70, Color.Red);
				}
				//Bitmap.Save($"smoothed_red_paths_{name}.png", ImageFormat.Png);
			}
		}

		private void FillBrightestTeeth()
		{
			(Color bc, Color sbc) = BrightestColor();
			visited = new bool[Width, Height];
			for (int x = 0; x < Width; x++)
			{
				int y = 0;
				while (y < Height && GetPixel(x, y).B != 255)
				{
					int a = GetPixel(x, y).R;
					if (a == bc.R)
					{
						FillColor(bc, sbc, x, y);
					}
					y++;
				}
			}
		}
		bool[,] visited;
		private void FillColor(Color bc, Color sbc, int startX, int startY)
		{
			Stack<Point> stack = new Stack<Point>();
			stack.Push(new Point(startX, startY));
			while (stack.Count > 0)
			{
				Point p = stack.Pop();

				if (p.X >= Width || p.Y >= Height)
					continue;
				if (p.X < 0 || p.Y < 0)
					continue;
				if (visited[p.X, p.Y])
					continue;
				Color cc = GetPixel(p.X, p.Y);
				if (cc.R == bc.R)
				{
					visited[p.X, p.Y] = true;

					SetPixel(p.X, p.Y, sbc);
					stack.Push(new Point(p.X + 1, p.Y));
					stack.Push(new Point(p.X, p.Y + 1));
					stack.Push(new Point(p.X, p.Y - 1));
					stack.Push(new Point(p.X - 1, p.Y));
				}
			}
		}
		private (Color, Color) BrightestColor()
		{
			int max = int.MinValue;
			int max2 = int.MinValue;
			for (int x = 0; x < Width; x++)
			{
				for (int y = 0; y < Height; y++)
				{
					if (GetPixel(x, y).R > max)
					{
						max2 = max;
						max = GetPixel(x, y).R;
					}
				}
			}
			return (Color.FromArgb(max, max, max), Color.FromArgb(max2, max2, max2));
		}

		public void FillTeeth(string name)
		{
			for (int x = 0; x < Width; x++)
			{
				int y = Height - 1;
				while (y >= 0 && GetPixel(x, y).R != 255 && GetPixel(x, y).B != 255)
				{
					SetPixel(x, y, Color.White);
					y--;
				}
				if (y == -1) continue;
				int startX = x;
				for (int i = -1; i < 2; i += 2)
				{
					x = startX;
					while (x >= 0 && x < Width && (GetPixel(x, y).R == 255 || GetPixel(x, y).B == 255))
					{
						SetPixel(x, y, Color.White);
						x += i;
					}
				}
				y--;
				x = startX;
				FillThisPossiblyTooth(startX, y);
			}
			//Bitmap.Save($"prefilled_{name}.png", ImageFormat.Png);

			List<(int x, int y)> edge = new List<(int x, int y)>();
			for (int x = 0; x < Width; x++)
			{
				int y = Height - 1;
				Color c = GetPixel(x, y);
				while (y >= Height / 2 && !(c.R == 0 && c.G == 128 && c.B == 0))
				{
					SetPixel(x, y, Color.White);
					y--;
					if (y >= 0)
					{
						c = GetPixel(x, y);
					}
				}
				edge.Add((x, y));
			}
			//Bitmap.Save($"half_done", ImageFormat.Png);

			double[] badsignal = new double[edge.Count];
			int j = 0;
			while (j < edge.Count)
			{
				int sumY = 0;
				int x = edge[j].x;
				int k = 0;
				while (j < edge.Count && edge[j].x == x)
				{
					sumY += edge[j].y;
					k++;
					j++;
				}
				badsignal[x] = sumY / k;
			}
			var signal = Vector.Create(badsignal);
			var smoothed = Smoothing.MovingAverage(badsignal, 5);
			edge.Clear();
			int l = 0;
			while (l < smoothed.Length && smoothed[l] > 0)
			{
				int y = smoothed[l] - (int)smoothed[l] > 0.5 ? (int)smoothed[l] + 1 : (int)smoothed[l];
				edge.Add((l, y));
				l++;
			}
			Clear();
			foreach ((int x, int y) in edge)
			{
				int yy = y;
				while (yy >= 0)
				{
					SetPixel(x, yy, Color.Green);
					yy--;
				}
			}
		}
		private void FillThisPossiblyTooth(int startX, int startY)
		{
			List<(int x, int y)> pixels = new List<(int x, int y)>();
			pixels.Add((startX, startY));
			bool nextFound = true;

			int y = startY;
			while (nextFound)
			{
				nextFound = false;
				int x = startX;
				while (y >= 0 && GetPixel(x, y).R != 255 && GetPixel(x, y).B != 255)
				{
					for (int i = -1; i < 2; i += 2)
					{
						x = startX + i;
						Color c = GetPixel(x, y);
						while (x >= 0 && x < Width && c.R != 255 && c.B != 255 && c.G != 255)
						{
							if (c.R != 0 || c.G != 0 || c.B != 0)
							{
								pixels.Add((x, y));
							}
							x += i;
							nextFound = true;
							if (x >= 0 && x < Width)
							{
								c = GetPixel(x, y);
							}
						}
					}
					y--;
				}
			}
			if (pixels.Count >= 150)
			{
				foreach ((int x, int yy) in pixels)
				{
					SetPixel(x, yy, Color.Green);
				}
			}
			else
			{
				foreach ((int x, int yy) in pixels)
				{
					SetPixel(x, yy, Color.White);
				}
			}
		}
		private void Clear()
		{
			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					SetPixel(i, j, Color.White);
				}
			}
		}
		public void Dispose()
		{
			if (Disposed) return;
			Disposed = true;
			Bitmap.Dispose();
			BitsHandle.Free();
		}
	}
}
