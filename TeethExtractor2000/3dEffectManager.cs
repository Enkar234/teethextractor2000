﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeethExtractor2000
{
	class _3DEffectManager
	{
		public PhongManager pm;
		public (double r, double g, double b) phongV;
		public _3DEffectManager() { }
		public void Paint(DirectBitmap bmp, int k, int j, int i, double V, Color? cc = null)
		{
			Color c = cc.HasValue ? cc.Value : Color.White;

			c = Color.FromArgb((int)(c.R * phongV.r), (int)(c.G * phongV.g), (int)(c.B * phongV.b));
			bmp.SetVPixel(k, j, c, V);
		}
		public void GridPaint(DirectBitmap bmp2, MyTriangle triangle, My3DPoint[] points, int bmpMinX, int bmpMinY, double v, int XOffset, int YOffset, Color c)
		{
			bmp2.DrawLine(new Point(triangle.p1.X - bmpMinX - XOffset, triangle.p1.Y - bmpMinY - YOffset), new Point(triangle.p2.X - bmpMinX - XOffset, triangle.p2.Y - bmpMinY - YOffset), c, v);
			bmp2.DrawLine(new Point(triangle.p2.X - bmpMinX - XOffset, triangle.p2.Y - bmpMinY - YOffset), new Point(triangle.p3.X - bmpMinX - XOffset, triangle.p3.Y - bmpMinY - YOffset), c, v);
			bmp2.DrawLine(new Point(triangle.p3.X - bmpMinX - XOffset, triangle.p3.Y - bmpMinY - YOffset), new Point(triangle.p1.X - bmpMinX - XOffset, triangle.p1.Y - bmpMinY - YOffset), c, v);
		}
	}
}
